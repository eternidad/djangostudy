from django.shortcuts import render
from django.http import HttpResponse


# Create your views here.
from blog.models import Article


def hello_world(request):
    return HttpResponse("HelloWorld!")

def article_con(request):
    article=Article.objects.all()[0]
    title=article.title
    brief_content=article.brief_content
    content=article.content
    article_id=article.article_id
    pub_date=article.pub_date
    return_str='title:%s<br>brief_content:%s<br>content:%s<br>' \
               'article_id:%s<br>pub_date:%s'%(title,
                                             brief_content,
                                             content,
                                             article_id,
                                             pub_date)
    return HttpResponse(return_str)

def get_indexPage(request):
    all_article=Article.objects.all()
    return render(request,'blog/index.html',
                  {
                      'article_list':all_article
                  })

def get_details(request,article_id):
    all_article=Article.objects.all()
    # curr_article=None

    for index,a in enumerate(all_article):
        if index==0:
            pre_index=index
            next_index=index+1
        elif index==len(all_article)-1:
            pre_index=index-1
            next_index=index
        else:
            pre_index=index-1
            next_index=index+1
        if a.article_id==article_id:
            curr_article=a
            pre_article=all_article[pre_index]
            next_article=all_article[next_index]
            break
    return render(request,'blog/detail.html',
                  {
                      'curr_article':curr_article,
                      'sels':curr_article.content.split('\n'),
                      'previous_article':pre_article,
                      'next_article':next_article
                  })
