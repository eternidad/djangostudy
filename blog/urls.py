#!/usr/bin/env python
# -*- coding:utf-8 -*-
from django.urls import path,include
import blog.views

urlpatterns=[
    path('hello_world',blog.views.hello_world),
    path('content',blog.views.article_con),
    path('index',blog.views.get_indexPage),
    # path('detail',blog.views.get_details),
    path('detail/<int:article_id>',blog.views.get_details)
]